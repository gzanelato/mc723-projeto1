import os
import time

FFT_VECTOR_DEGREE = 2000000
NN_DATA_ENTRIES = 300
ADJUST_CONSTANT = 7050

def compile(progName):
	os.system("g++ -O2 -std=c++11 -o " + progName + " " + progName + ".cpp")

def score(t1, t2):
	return int(round(1./(t1 + t2)*ADJUST_CONSTANT))

def eraseExtraFiles():
	os.system("rm fft fft.out training_nn model-neural-network.dat")

def evaluateFFT():
	start = time.time()
	os.system("./fft " + str(FFT_VECTOR_DEGREE))
	end = time.time()
	return end - start

def evaluateNN():
	start = time.time()
	os.system("./training_nn " + str(NN_DATA_ENTRIES))
	end = time.time()
	t2 = end - start
	return end - start

def getAvgWithoutWithoutEndpoints(n, method, label):
	runs = []
	for i in range(n):
		t = method()
		print(label + ' exec #' + str(i+1) + ': ' + '%.3f'%t + 's')
		runs.append(t)
	runs.sort()
	tot = sum(runs[1:len(runs)-1])
	return tot/(n-2.0)

def main():
	compile('fft')
	compile('training_nn')
	print('Degree of each polynomial: ' + str(FFT_VECTOR_DEGREE))
	t1 = getAvgWithoutWithoutEndpoints(5, evaluateFFT, 'FFT')
	print("\n|NN's training set|: " + str(NN_DATA_ENTRIES))
	t2 = getAvgWithoutWithoutEndpoints(5, evaluateNN, 'NN')
	print("\nscore = " + str(score(t1,t2)) + ' (the more, the better)')
	eraseExtraFiles()
	
if __name__ == "__main__":
	main()

