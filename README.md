# Programa 1 - FFT e Neural Network com MNIST database - Grupo 7

## O que faz? Para que serve?
Fast Fourier Transform (FFT) é um algoritmo que permite calcular a transformada discreta de Fourier. Esta por sua vez apresenta diversas aplicações, especialmente na análise de sinais. A nossa aplicação a utliza para calcular a convolução de dois vetores.
Redes neurais (NN) estão ganhando um espaço cada vez maior em Aprendizado de Máquina, uma aplicação onde elas têm se mostrado particularmente efetivas é na análise de imagens. Uma avaliação clássica da qualidade de uma NN é através do seu desempenho para reconhecer digitos escritos a mão de uma base de dados do Mixed National Institute of Standards and Technology (MNIST).

## Por que é bom para medir desempenho?
Nossa aplicação possibilita uma boa avaliação do desempenho da CPU, disco e memória pois FFT faz um uso muito intenso da CPU, enquanto o treinamento da NN possibilita a análise do uso do CPU e da memória quando este faz acesso a novos data entries. Além disso, ambas as rotinas escrevem seus resultados em disco no fim. 

## O que baixar
É suficiente clonar este repositório.

## Como compilar/instalar
Não é necessário.

## Como executar
Basta executar o arquivo 'benchmark.py'. (E.g. Python2 'python benchmark.py', Python3 'python3 benchmark.py')

## Como medir o desempenho
Tanto a FFT como o treinamento da NN demandam grande quantidade de processamento da CPU, sendo assim, determinamos que o tempo para executar cada programa dada uma entrada de tamanho suficientemente grande seria uma boa medida para calcular o desempenho.

Medimos o tempo de execução para a FFT utilizando um polinômio de grau 20000000 e repetimos o processo 5 vezes descartando o maior e menor valor com o intuito de diminuir nosso erro e calculando a média entre os valores não descartados. O mesmo foi feito para o treinamento da rede neural usando um conjunto de treinamento de 300 entradas.

Dessa forma, com ambas as médias podemos as usar como parâmetros para determinar o desempenho da máquina. Utilizamos um valor denominado **score** para representar o desempenho da máquina, tal valor é dado pela seguinte formula: 

Score = (1./(tFFT + tNN)*CONSTANTE)  - Esse valor deve ser arredondado para o inteiro mais próximo.

Onde tFFT é a média do tempo de execução da FFT, tNN é a média do tempo de execução do treinamento da NN e CONSTANTE é uma constante utilizada para deixar o resultado na casa das centenas, tornando o resultado mais palpável e mais fácil de visualizar ao realizar comparações.

## Como apresentar o desempenho
O desempenho da máquina é dado através do **score** já citado acima que junto com outras informações da execução do benchmark são impressas na tela ao final da execução do arquivo benchmark.py.

Temos abaixo como referência as especificações de uma máquina e o valor do score obtido.

## Medições base (uma máquina)
### Config ###

 **Processor Name:**	Intel Core i5

  **Processor Speed:**	2.9 GHz

  **Number of Processors:**	1

  **Total Number of Cores:** 2

  **L2 Cache (per Core):**	256 KB

  **L3 Cache:**	3 MB

### Memory ###

   **2x  Size:**	8 GB

   **Type:**	DDR3

   **Speed:**	1867 MHz

### Score = 1004 ###